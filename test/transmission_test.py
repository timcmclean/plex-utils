#!/usr/bin/env python

import transmissionrpc
tc = transmissionrpc.Client('qnap', port=9091, user="admin", password="admin")

for torrent in tc.get_torrents():
    print torrent.name + " - Completed in: " + str(torrent.leftUntilDone)
    if torrent.leftUntilDone == 0:
        tc.stop_torrent(torrent.id)
        tc.remove_torrent(torrent.hashString)
        print "Download completed - stopped and removed " + str(torrent.hashString)
