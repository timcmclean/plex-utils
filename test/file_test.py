#!/usr/bin/env python

import sys, paramiko, time, httplib, urllib
from plexapi.server import PlexServer

hostname = "qnap"
username = "admin"
password = "admin"
port = 22

download_folder = "/share/Download/testing/from/"
media_folder = "/share/Download/testing/to/"

has_files = False

# File movement activities
print time.strftime("%c") + " - Checking if any files are ready for movement..."
try:
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.WarningPolicy())

    client.connect(hostname, port=port, username=username, password=password)

    stdin, stdout, stderr = client.exec_command("ls " + download_folder)
    file_list = []
    for file in stdout.readlines():
        print file
        file_list.append(file.strip())
        has_files = True

    if has_files:
        stdin, stdout, stderr = client.exec_command("mv " + download_folder + "* " + media_folder + ".")
    print stdout.read()

finally:
    client.close()

# Send notification
conn = httplib.HTTPSConnection("api.pushover.net:443")
for file in file_list:
    conn.request("POST", "/1/messages.json",
      urllib.urlencode({
        "token": "ab3EabVw8o49A4ecEwgzWLMcL1Q8R6",
        "user": "urhj7qsSTgvPe9gdm7cr9MbEmS1F2w",
        "message": "File %s is ready" % file,
      }), { "Content-type": "application/x-www-form-urlencoded" })
    r = conn.getresponse()
    r.read()
