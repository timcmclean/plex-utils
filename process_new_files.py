#!/usr/bin/env python

import sys, paramiko, transmissionrpc, time, httplib, urllib
from plexapi.server import PlexServer


hostname = "qnap"
username = "admin"
password = "admin"
port = 22

download_folder = "/share/Download/transmission/tv/"
media_folder = "/share/Multimedia/TV/"

baseurl = 'http://plex:32400'
token = '1878aXLb5XsFzY6Dvpqs'

has_files = False


# Transmission activities
print time.strftime("%c") + " - Checking transmission for latest downloads..."
tc = transmissionrpc.Client(hostname, port=9091, user=username, password=password)

for torrent in tc.get_torrents():
    print torrent.name + " - Completed in: " + str(torrent.leftUntilDone)
    if torrent.leftUntilDone == 0:
        tc.stop_torrent(torrent.id)
        tc.remove_torrent(torrent.hashString)
        print "Download completed - stopped and removed " + str(torrent.hashString)



# File movement activities
print time.strftime("%c") + " - Checking if any files are ready for movement..."
try:
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.WarningPolicy())

    client.connect(hostname, port=port, username=username, password=password)

    stdin, stdout, stderr = client.exec_command("ls " + download_folder)
    file_list = []
    for file in stdout.readlines():
        print file
        file_list.append(file.strip())
        has_files = True

    if has_files:
        stdin, stdout, stderr = client.exec_command("mv " + download_folder + "* " + media_folder + ".")
    print stdout.read()

finally:
    client.close()


if has_files:
    # Refresh Plex
    print time.strftime("%c") + " - Refreshing Plex..."
    plex = PlexServer(baseurl, token)

    plex.library.refresh()

    print time.strftime("%c") + " - Sending notifications..."

    conn = httplib.HTTPSConnection("api.pushover.net:443")
    for file in file_list:
        conn.request("POST", "/1/messages.json",
          urllib.urlencode({
            "token": "ab3EabVw8o49A4ecEwgzWLMcL1Q8R6",
            "user": "urhj7qsSTgvPe9gdm7cr9MbEmS1F2w",
            "message": "File %s is ready" % file,
          }), { "Content-type": "application/x-www-form-urlencoded" })
        r = conn.getresponse()
        r.read()
